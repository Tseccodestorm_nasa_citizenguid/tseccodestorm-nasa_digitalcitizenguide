package com.androidprojects.solankinilesh.nasa.Objects;

import com.android.volley.toolbox.StringRequest;

public class Result {

    String name, place_id, lat, lng, vicinity, phone;
    boolean isOpenNow;
    double rating;

    public Result(String name, String place_id, String lat, String lng, String vicinity, String phone, boolean isOpenNow, double rating) {
        this.name = name;
        this.place_id = place_id;
        this.lat = lat;
        this.lng = lng;
        this.vicinity = vicinity;
        this.phone = phone;
        this.isOpenNow = isOpenNow;
        this.rating = rating;
    }


    public double getRating() {
        return rating;
    }

    public String getName() {

        return name;
    }

    public String getPlace_id() {
        return place_id;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isOpenNow() {
        return isOpenNow;
    }

    public String getLat() {
        return lat;

    }

    public String getLng() {
        return lng;
    }

    public String getVicinity() {
        return vicinity;
    }
}
