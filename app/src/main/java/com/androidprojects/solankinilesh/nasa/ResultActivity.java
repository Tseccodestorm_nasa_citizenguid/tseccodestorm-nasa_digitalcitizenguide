package com.androidprojects.solankinilesh.nasa;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidprojects.solankinilesh.nasa.Adapters.ResultAdapter;
import com.androidprojects.solankinilesh.nasa.Adapters.TypeAdapter;
import com.androidprojects.solankinilesh.nasa.Helpers.Constants;
import com.androidprojects.solankinilesh.nasa.Objects.Result;
import com.androidprojects.solankinilesh.nasa.Objects.ServiceType;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ResultActivity extends AppCompatActivity implements LocationListener {

    private final static int PLACE_PICKER_REQUEST = 999;
    GoogleSignInClient mSignInClient;
    LocationManager locationManager;
    BottomSheetBehavior sheetBehavior;
    ListView typesList;
    ConstraintLayout bottomSheetLayout;
    List<ServiceType> types = new ArrayList<>();
    List<Result> resultList = new ArrayList<>();
    TypeAdapter adapter;
    ResultAdapter resultAdapter;
    TextView typeHead;
    ProgressBar progressBar;
    private Double lat,longi;
    final static int REQ_CODE = 99;
    ListView resultsListView;
    RequestQueue requestQueue;
    final String RESPONSE_TAG = "tag";
    Toolbar toolbar;
    private TextView Tvshow;

    @Override
    protected void onStop() {
        super.onStop();

        if (requestQueue != null) {
            requestQueue.cancelAll(RESPONSE_TAG);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.result_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.logout:
                mSignInClient.signOut();
                Intent intent = new Intent(ResultActivity.this, MainActivity.class);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getLocation();
    }

    private void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 5, this);

        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Salvager");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        typeHead = findViewById(R.id.tv_type_head);
        typesList = findViewById(R.id.types_list);
        bottomSheetLayout = findViewById(R.id.bottom_sheet_list);
        sheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
        progressBar = findViewById(R.id.progressBar);
        mSignInClient = GoogleSignIn.getClient(this, gso);
        resultsListView = findViewById(R.id.lv_results);
        Tvshow=(TextView)findViewById(R.id.tvshow);

        typesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                TextView typeName = view.findViewById(R.id.tv_type_name);
                typeHead.setText(typeName.getText().toString());
                typeHead.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);


                loadEntities(position, lat, longi);

            }
        });

        fillList();


        requestLocation();




        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
//                        progressBar.setVisibility(View.VISIBLE);
//                        resultsListView.setVisibility(View.INVISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        resultsListView.setVisibility(View.INVISIBLE);
                        typeHead.setVisibility(View.INVISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Tvshow.setVisibility(View.INVISIBLE);
                        resultsListView.setVisibility(View.INVISIBLE);
                        typeHead.setVisibility(View.INVISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

//    private void loadFakeEntities(String response) {
//
//
//        JSONObject root = null;
//        try {
//            root = new JSONObject(response);
//            JSONArray results = root.getJSONArray("results");
//
//            for(int i = 0; i < results.length()-1; i++){
//                JSONObject res = results.getJSONObject(i);
//                JSONObject location = res.getJSONObject("geometry").getJSONObject("location");
//                resultList.add(new Result(
//                        res.getString("name"),
//                        res.getString("place_id"),
//                        location.getDouble("lat")+"",
//                        location.getDouble("lng") + "",
//                        res.getString("vicinity"),
//                        "",
//                        false,
//                        res.getDouble("rating")
//                ));
//            }
//
////            if(resultAdapter != null ){resultAdapter.clear();}
//            resultAdapter = new ResultAdapter(ResultActivity.this, resultList);
//            resultsListView.setVisibility(View.VISIBLE);
//            resultsListView.setAdapter(resultAdapter);
//
//        } catch (JSONException e1) {
//            e1.printStackTrace();
//        }
//
//    }


    private void loadEntities(final int position,final Double lat,final Double longi) {

        if(resultAdapter != null){resultAdapter.clear();}
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                Constants.urls[position],
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {


                            JSONObject root = new JSONObject(response);
                            Log.i(ResultActivity.class.getSimpleName(), "onResponse: "+response);
                            JSONArray results = root.getJSONArray("results");
                            if(results.length() == 0){

                                Toast.makeText(ResultActivity.this, "Nothing Found ", Toast.LENGTH_SHORT).show();
                                if (requestQueue != null) {

                                    requestQueue.cancelAll(RESPONSE_TAG);
                                }
                            }
                            progressBar.setVisibility(View.INVISIBLE);
//                            resultsListView.setText(results.toString());
                            for(int i = 0; i < results.length()-1; i++){
                                JSONObject res = results.getJSONObject(i);
                                JSONObject location = res.getJSONObject("geometry").getJSONObject("location");
                                resultList.add(new Result(
                                        res.getString("name"),
                                        res.getString("place_id"),
                                        location.getDouble("lat")+"",
                                        location.getDouble("lng") + "",
                                        res.getString("vicinity"),
                                        "",
                                        true,
                                        0
                                ));
                            }
                            resultAdapter = new ResultAdapter(ResultActivity.this, resultList);
                            resultsListView.setVisibility(View.VISIBLE);
                            resultsListView.setAdapter(resultAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ResultActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        loadEntities(position, lat, longi);
                    }
                }
        );
        stringRequest.setTag(RESPONSE_TAG);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQ_CODE && permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

            getLocation();

        }
    }



    private void requestLocation() {

        if(Build.VERSION.SDK_INT > 22){

            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {



                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                    Toast.makeText(this, "Please allow permissions!", Toast.LENGTH_SHORT).show();
                }
                else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQ_CODE);
                }

            }
            else{

                getLocation();
            }

        }else {
            getLocation();
        }


    }

    private void fillList() {

        types.add(new ServiceType(R.drawable.ic_airportdp, "Airport"));
        types.add(new ServiceType(R.drawable.ic_policedp, "Police Station"));
        types.add(new ServiceType(R.drawable.ic_hospitaldp, "Hospital"));
        types.add(new ServiceType(R.drawable.ic_fire_brigadedp, "Fire Station"));
        types.add(new ServiceType(R.drawable.ic_bank, "Bank"));
        types.add(new ServiceType(R.drawable.ic_car_repairdp, "Car Repair"));

        adapter = new TypeAdapter(this, types);
        typesList.setAdapter(adapter);

    }


    @Override
    public void onLocationChanged(Location location) {
        lat=location.getLatitude();
        String l1=String.valueOf(lat);
        longi=location.getLongitude();
        String l2=String.valueOf(longi);
        Toast.makeText(getApplicationContext(),"You latitude and longitude are "+l1 +" and "+l2,Toast.LENGTH_SHORT).show();
        Log.d(MainActivity.class.getSimpleName(), "You latitude and longitude are "+l1 +" and "+l2);

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

        }catch(Exception e)
        {

        }

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(ResultActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}
