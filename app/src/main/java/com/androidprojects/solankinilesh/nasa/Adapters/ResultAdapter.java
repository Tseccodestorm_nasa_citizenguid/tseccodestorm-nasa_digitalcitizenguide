package com.androidprojects.solankinilesh.nasa.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.androidprojects.solankinilesh.nasa.MapActivity;
import com.androidprojects.solankinilesh.nasa.Objects.Result;
import com.androidprojects.solankinilesh.nasa.R;
import com.androidprojects.solankinilesh.nasa.ResultActivity;
import com.androidprojects.solankinilesh.nasa.StartActivity;

import java.util.List;

public class ResultAdapter extends ArrayAdapter<Result> {

    List<Result> resultList;
    Context context;

    public ResultAdapter(@NonNull Context context,List<Result> resultList ) {
        super(context, R.layout.result_list_item, resultList);
        this.context = context;
        this.resultList = resultList;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.result_list_item, parent, false);

        TextView name, vicinity, rating, isOpenNow;
        name = v.findViewById(R.id.result_name);
        vicinity = v.findViewById(R.id.result_address);
        rating = v.findViewById(R.id.result_rating);
        isOpenNow = v.findViewById(R.id.result_isOpen);
        v.findViewById(R.id.btn_maps).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //place picker
                Intent map = new Intent(context, MapActivity.class);
                map.putExtra("lat", Double.parseDouble(resultList.get(position).getLat()));
                map.putExtra("lng", Double.parseDouble(resultList.get(position).getLng()));
                context.startActivity(map);
            }
        });

        name.setText(resultList.get(position).getName());
        vicinity.setText(resultList.get(position).getVicinity());
        rating.setText(resultList.get(position).getRating() + "");
        isOpenNow.setText(resultList.get(position).isOpenNow() + "");

        return  v;

    }
}
