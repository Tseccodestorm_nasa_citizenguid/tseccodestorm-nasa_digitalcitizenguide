package com.androidprojects.solankinilesh.nasa.Helpers;

public class Constants {


    public Constants() {
    }

    public static String getURLPlaces(String type, Double lat, Double lng){

//        return "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input="+type+"&inputtype=textquery&fields=formatted_address,name,opening_hours,rating&locationbias=circle:10000@"+lat+","+lng+"&key=AIzaSyAInH3DacPBzN3oxa9zKOWPgoyX-nHdWnw";
            return "https://maps.googleapis.com/maps/api/place/search/json?location="+lat+","+lng+"&radius=5000&types="+type+"&sensor=false&key=AIzaSyAInH3DacPBzN3oxa9zKOWPgoyX-nHdWnw";

//        return " https://maps.googleapis.com/maps/api/place/search/json?location"+lat+","+lng+"rankby=distance&types="+type+"&sensor=false&key=AIzaSyAwr2xqNH8yyUeutSH98pqQJcFRm7-2AGA";
    }

    public static String getURLPlaceDetails(String place_id){

        return "\n" +
                "https://maps.googleapis.com/maps/api/place/details/json?placeid="+place_id+"&fields=formatted_phone_number,website&key=AIzaSyAInH3DacPBzN3oxa9zKOWPgoyX-nHdWnw";

    }


    public static final String[] typesForUrl = {"airport","police", "hospital", "fire_station", "bank", "car_repair"};


    public static String airport_url = "https://api.myjson.com/bins/1b1krw";
    public static String police_url = "https://api.myjson.com/bins/bz58s";
    public static String hospital_url = "https://api.myjson.com/bins/6m8ss";
    public static String firestation_url = "https://api.myjson.com/bins/d60gc";
    public static String bank_url = "https://api.myjson.com/bins/kb7po";
    public static String carrepair_url = "https://api.myjson.com/bins/kwnbg";

    public static final String[] urls = {"https://api.myjson.com/bins/1b1krw",
            "https://api.myjson.com/bins/bz58s",
            "https://api.myjson.com/bins/6m8ss",
            "https://api.myjson.com/bins/d60gc",
            "https://api.myjson.com/bins/kb7po",
            "https://api.myjson.com/bins/kwnbg"};
    
}
