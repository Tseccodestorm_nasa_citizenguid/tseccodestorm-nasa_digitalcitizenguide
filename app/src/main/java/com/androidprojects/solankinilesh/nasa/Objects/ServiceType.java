package com.androidprojects.solankinilesh.nasa.Objects;

import android.widget.ImageView;

public class ServiceType {

    String typeName;
    int typeIcon;

    public ServiceType(int typeIcon, String typeName) {
        this.typeIcon = typeIcon;
        this.typeName = typeName;
    }

    public int getTypeIcon() {
        return typeIcon;
    }

    public String getTypeName() {
        return typeName;
    }
}
