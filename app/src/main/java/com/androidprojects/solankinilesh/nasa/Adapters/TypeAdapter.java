package com.androidprojects.solankinilesh.nasa.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidprojects.solankinilesh.nasa.Objects.ServiceType;
import com.androidprojects.solankinilesh.nasa.R;

import java.util.List;

public class TypeAdapter extends ArrayAdapter<ServiceType> {

    Context context;
    List<ServiceType> serviceTypeList;

    public TypeAdapter(@NonNull Context context, List<ServiceType> serviceTypeList) {
        super(context, R.layout.list_item_bottom_sheet,serviceTypeList);
        this.context = context;
        this.serviceTypeList = serviceTypeList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.list_item_bottom_sheet, parent, false);

        TextView typeName = v.findViewById(R.id.tv_type_name);
        ImageView typeImage = v.findViewById(R.id.iv_type_icon);

        typeImage.setImageResource(serviceTypeList.get(position).getTypeIcon());
        typeName.setText(serviceTypeList.get(position).getTypeName());

        return v;
    }
}
